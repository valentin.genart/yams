package main;

/**
 * Ce Thread permet le defilement de nombre al�atoire lors du tirage al�atoire
 * @author Valentin
 *
 */
public class Thread_De implements Runnable{

	private Interface inter;
	
	public Thread_De(Interface inter) {
		super();
		this.inter = inter;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		inter.runThreadSync();
	}

}
