package main;
/**
 * Classe repr�sentant un joueur avec son nom, son initiale et son tableau de scores.
 * @author Valentin
 *
 */
public class Joueur {
	
	/**
	 * Le nom du joueur.
	 */
	private String name;
	
	/**
	 * L'initiale du joueur.
	 */
	private char initiale;
	
	/**
	 * Le tableau de scores du joueur.
	 */
	private int[] scores;
	
	/**
	 * Le num�ro du joueur dans la partie.
	 */
	private int numero;
	
	/**
	 * Constructeur sp�cifiant le nom du joueur.
	 * @param name le nom du joueur.
	 */
	public Joueur(String name, int numero) {
		super();
		this.numero = numero;
		this.name = name;
		this.initiale = name.toUpperCase().charAt(0);
		this.scores = new int[17];
		for (byte i = 0; i < scores.length; i++) {
			scores[i] = -1;
		}
	}
	
	/**
	 * Retourne le tableau de scores du joueur.
	 * @return le tableau de scores du joueur
	 */
	public int[] getScores() {
		return scores;
	}
	
	/**
	 * D�finit un score dans le tableau de scores du joueur, � la position sp�cifi�e dans le tableau.
	 * @param position l'index du score � ins�rer
	 * @param score le score � ins�rer
	 */
	public void setScore(int position, int score) {
		this.scores[position] = score;
	}
	
	/**
	 * Retourne le nom du joueur.
	 * @return le nom du joueur
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Retourne l'initiale du joueur.
	 * @return l'initiale du joueur.
	 */
	public char getInitiale() {
		return initiale;
	}
	
	/**
	 * Retourne le num�ro du joueur dans la partie.
	 * @return le num�ro du joueur dans la partie.
	 */
	public int getNumero() {
		return numero;
	}
}
