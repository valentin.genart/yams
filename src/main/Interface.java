package main;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JOptionPane;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Classe repr�sentant l'interface graphique du jeu.
 * @author Valentin
 *
 */
public class Interface extends Application{

	/**
	 * Le contexte graphique permettant d'afficher la grille dans l'interface.
	 */
	private GraphicsContext gc;

	/**
	 * Le compteur des lanc�s de d�s du joueur courant, r�initialis� pour chaque joueur.
	 */
	private int compteurLances;

	/**
	 * Tableau contenant les �l�ments Text repr�sentant le r�sultat des 5 d�s.
	 */
	private Text[] tabTextDes;

	private Canvas[] tabCanvasDes;

	private GraphicsContext[] tabGCDes;

	/**
	 * Tableau contenant les �l�ments Text repr�sentant le score pour chaque figure obtenu par le lancer
	 * des 5 d�s.
	 */
	private Text[] tabTextResultat;

	/**
	 * Le joueur dont c'est au tour de jouer.
	 */
	private Joueur joueurCourant;

	/**
	 * Le nombre de joueurs total de la partie.
	 */
	private int nbJoueurs;

	/**
	 * Tableau contenant tous les joueurs de la partie.
	 */
	private Joueur[] tabJoueurs;

	/**
	 * Le bouton permettant de lancer les d�s.
	 */
	private Button lancer;

	/**
	 * Tableau contenant les 13 boutons repr�sentant chaque figure.
	 */
	private Button[] tabButtonResultat;

	/**
	 * Le texte affichant le nom du joueur courant
	 */
	private Text textNomJoueur;

	/**
	 * Tableau contenant les 5 checkbox permettant de selectionner les d�s � garder.
	 */
	private CheckBox[] tabCheckBox;

	/**
	 * Le texte affichant le nombre de lanc�s restants.
	 */
	private Text textNblancers;

	/**
	 * M�thode affichant l'interface graphique du jeu.
	 */
	@Override
	public void start(Stage stage) throws Exception {
		nbJoueurs = nombreJoueurs();
		tabJoueurs = new Joueur[nbJoueurs];
		for (byte i = 0; i < nbJoueurs; i++)
			tabJoueurs[i] = nouveauJoueur(i+1);
		joueurCourant = tabJoueurs[0];
		HBox root = new HBox();
		Scene scene = new Scene(root, 1020, 600);
		Canvas c = new Canvas(400, 600);
		gc = c.getGraphicsContext2D();
		VBox vboxDes = new VBox();
		VBox vboxResultats = new VBox();
		VBox vboxScores = new VBox();
		Separator sep1 = new Separator(Orientation.VERTICAL);
		Separator sep2 = new Separator(Orientation.VERTICAL);
		root.getChildren().addAll(vboxDes, sep1, vboxResultats, sep2, vboxScores);
		textNomJoueur = new Text("Au tour de : "+joueurCourant.getName());
		HBox[] tabHboxDes = new HBox[5];
		tabCheckBox = new CheckBox[5];
		tabCanvasDes = new Canvas[5];
		tabTextDes = new Text[5];
		tabGCDes = new GraphicsContext[5];
		for (byte i = 0; i < tabCheckBox.length; i++) {
			tabHboxDes[i] = new HBox();
			tabHboxDes[i].setSpacing(27);
			tabCheckBox[i] = new CheckBox();
			tabTextDes[i] = new Text("0");
			tabCanvasDes[i] = new Canvas(30, 30);
			tabGCDes[i] = tabCanvasDes[i].getGraphicsContext2D();
			tabHboxDes[i].getChildren().addAll(tabCanvasDes[i], tabCheckBox[i]);
		}
		HBox hboxLancer = new HBox();
		lancer = new Button("Lancer");
		lancer.setDefaultButton(true);
		lancer.setPrefWidth(73);
		compteurLances = 3;
		textNblancers = new Text(compteurLances+" lancés restants");
		hboxLancer.setSpacing(10);
		hboxLancer.getChildren().addAll(lancer, textNblancers);
		Button aide = new Button("Aide");
		aide.setPrefWidth(73);
		Image image = new Image(new File("ressources/aide.png").toURI().toString());
		ImageView imgView = new ImageView(image);
		imgView.setFitHeight(17);
		imgView.setPreserveRatio(true);
		aide.setGraphic(imgView);
		Image yam = new Image(new File("ressources/yam.jpg").toURI().toString());
		ImageView imageYam = new ImageView(yam);
		imageYam.setFitWidth(240);
		imageYam.setPreserveRatio(true);
		VBox.setMargin(imageYam, new Insets(0, 0, 30, 0));
		vboxDes.getChildren().addAll(imageYam, textNomJoueur, tabHboxDes[0], tabHboxDes[1], tabHboxDes[2], tabHboxDes[3], tabHboxDes[4], hboxLancer, aide);
		vboxDes.setPrefWidth(300);
		vboxDes.setPadding(new Insets(30));
		vboxDes.setSpacing(10);
		//2EME PARTIE
		HBox[] tabHboxResultat = new HBox[13];
		tabButtonResultat = new Button[13];
		tabTextResultat = new Text[13];
		for (byte i = 0; i < tabHboxResultat.length; i++) {
			tabHboxResultat[i] = new HBox();
			tabHboxResultat[i].setSpacing(20);
			tabButtonResultat[i] = new Button();
			tabButtonResultat[i].setPrefWidth(200);
			tabTextResultat[i] = new Text("0");
			tabHboxResultat[i].getChildren().addAll(tabButtonResultat[i], tabTextResultat[i]);
			vboxResultats.getChildren().add(tabHboxResultat[i]);
		}
		for (byte i = 0; i < 6; i++)
			tabButtonResultat[i].setText("Total des "+(i+1));
		tabButtonResultat[6].setText("Brelan");
		tabButtonResultat[7].setText("Full");
		tabButtonResultat[8].setText("Carre");
		tabButtonResultat[9].setText("Petite suite");
		tabButtonResultat[10].setText("Grande suite");
		tabButtonResultat[11].setText("Yams");
		tabButtonResultat[12].setText("Chance");
		vboxResultats.setPadding(new Insets(80, 30, 80, 30));
		vboxResultats.setPrefWidth(300);
		vboxResultats.setSpacing(5);
		//3E PARTIE
		vboxScores.getChildren().add(c);
		gc.setLineWidth(1);
		gc.strokeRect(10, 10, 380, 551);
		gc.strokeLine(160, 10, 160, 561);
		for (byte i = 0; i < 17; i++)
			gc.strokeLine(10, 10+(i+1)*31, 390, 10+(i+1)*31);
		for (byte i = 0; i < nbJoueurs; i++) {
			gc.strokeLine(170+(i+1)*(220/nbJoueurs), 10, 170+(i+1)*(220/nbJoueurs), 561);
			gc.strokeText(""+tabJoueurs[i].getInitiale(), 170+(i+1)*(220/nbJoueurs)-((i+1)*(220/nbJoueurs)-i*220/nbJoueurs)/2, 30);
		}
		byte ligne = 0;
		gc.setStroke(Color.BLACK);
		gc.strokeText("Total des 1", 30, 30+ ++ligne*31);
		gc.strokeText("Total des 2", 30, 30+ ++ligne*31);
		gc.strokeText("Total des 3", 30, 30+ ++ligne*31);
		gc.strokeText("Total des 4", 30, 30+ ++ligne*31);
		gc.strokeText("Total des 5", 30, 30+ ++ligne*31);
		gc.strokeText("Total des 6", 30, 30+ ++ligne*31);
		gc.setStroke(Color.GREEN);
		gc.strokeText("Prime des 63 (35pts)", 30, 30+ ++ligne*31);
		gc.setStroke(Color.RED);
		gc.strokeText("Total Haut", 30, 30+ ++ligne*31);
		gc.setStroke(Color.BLACK);
		gc.strokeText("Brelan (total dés)", 30, 30+ ++ligne*31);
		gc.strokeText("Full (25pts)", 30, 30+ ++ligne*31);
		gc.strokeText("Carré (total dés)", 30, 30+ ++ligne*31);
		gc.strokeText("Petite suite (30pts)", 30, 30+ ++ligne*31);
		gc.strokeText("Grande suite (40pts)", 30, 30+ ++ligne*31);
		gc.strokeText("Yams (50pts)", 30, 30+ ++ligne*31);
		gc.strokeText("Chance (total dés)", 30, 30+ ++ligne*31);
		gc.setStroke(Color.RED);
		gc.strokeText("Total Bas", 30, 30+ ++ligne*31);
		gc.strokeText("Total Général", 30, 30+ ++ligne*31);
		gc.setStroke(Color.BLACK);
		
		aide.setOnAction(e->{
			Stage stageAide = new Stage();
			stageAide.initOwner(stage); //D�finit la fen�tre principale comme fen�tre parente.
			stageAide.initModality(Modality.WINDOW_MODAL); //Verrouille la fen�tre parente.
			VBox rootAide = new VBox();
			Scene sceneAide = new Scene(rootAide, 920, 400);
			stageAide.setTitle("Aide");
			stageAide.setScene(sceneAide);
			Text textAide = new Text("Lorsque c'est votre tour, lancez les dés. A chaque lancé de dés, choisissez les dés que vous souhaitez garder en cochant les cases associées. Relancez ensuite les autres dés.\n"
					+ "Le but est de remplir sa grille de points en effectuant chacune des figures et en maximisant les points pour chacune.\n"
					+ "\n"
					+ "Partie haute de la grille:\n"
					+ "Cette partie contient les scores obtenus pour chaque chiffre.\n"
					+ "Exemple: Total des 3: Somme des 3 obtenus.\n"
					+ "Si vous obtenez au moins 63 points dans cette partie, un bonus de 35 points vous est attribué.\n"
					+ "\n"
					+ "Partie basse de la grille:\n"
					+ "Brelan: 3 dés du même chiffre. Il vaut 3 fois le chiffre obtenu.\n"
					+ "Full: 3 dés d'un même chiffre + 2 dés d'un autre chiffre. Il vaut toujours 25 points.\n"
					+ "Carré: 4 dés du même chiffre. Il vaut 4 fois le chiffre obtenu.\n"
					+ "Petite suite: 1 2 3 4 5. Elle vaut toujours 30 points.\n"
					+ "Grande suite: 2 3 4 5 6. Elle vaut toujours 40 points.\n"
					+ "Yam's: 5 dés du même chiffre. Il vaut toujours 50 points.\n"
					+ "Chance: Somme des chiffres obtenus.\n");
			textAide.setWrappingWidth(sceneAide.getWidth()-10); //Adapte le texte � la largeur de la fen�tre.
			textAide.setLineSpacing(5);
			textAide.setTextAlignment(TextAlignment.JUSTIFY);
			Button okAide = new Button("J'ai compris !");
			okAide.setDefaultButton(true);
			rootAide.getChildren().addAll(textAide, okAide);
			okAide.setOnAction(e2->{
				stageAide.close();
			});
			rootAide.setPadding(new Insets(5));
			rootAide.setSpacing(5);
			stageAide.setResizable(false);
			stageAide.show();
		});
		
		lancer.setOnAction(e->{
			compteurLances--;
			if(compteurLances>1)
				textNblancers.setText(compteurLances+" lancés restants");
			else
				textNblancers.setText(compteurLances+" lancé restant");
			//Non fonctionnel: bug random dans le thread: Exception in thread "JavaFX Application Thread" java.lang.IndexOutOfBoundsException: Index -1 out of bounds for length 5
			/*for (byte j = 0; j < 10; j++) {
					try {
						Thread t = new Thread(new Thread_De(this));
						t.start();
					}catch(Exception ex) {
						lancer.setDisable(true);
						for (byte i = 0; i < 5; i++) {
							if(!tabCheckBox[i].isSelected()) {
								byte nb_tire=tirage();
								tabTextDes[i].setText(""+nb_tire);
								tabGCDes[i].drawImage(new Image(new File("ressources/de_"+nb_tire+".png").toURI().toString()), 0, 0, 20, 20);
							}
						}
						calculResultats();
						lancer.setDisable(false);
						if(compteurLances < 1)
							lancer.setDisable(true);
					}
			}*/
			//Du coup: Affichage sans le d�filement de nombre al�atoire
			if(compteurLances < 1)
				lancer.setDisable(true);
			for (int i = 0; i < 5; i++) {
				if(!tabCheckBox[i].isSelected()) {
					byte nb_tire=tirage();
					tabTextDes[i].setText(""+nb_tire);
					tabGCDes[i].drawImage(new Image(new File("ressources/de_"+nb_tire+".png").toURI().toString()), 0, 0, 30, 30);
				}
			}
			calculResultats();
		});

		tabCheckBox[0].setOnAction(e->{
			actionCheckBox();
		});
		tabCheckBox[1].setOnAction(e->{
			actionCheckBox();
		});
		tabCheckBox[2].setOnAction(e->{
			actionCheckBox();
		});
		tabCheckBox[3].setOnAction(e->{
			actionCheckBox();
		});
		tabCheckBox[4].setOnAction(e->{
			actionCheckBox();
		});
		tabButtonResultat[0].setOnAction(e->{
			joueurCourant.setScore(0, Integer.parseInt(tabTextResultat[0].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[1].setOnAction(e->{
			joueurCourant.setScore(1, Integer.parseInt(tabTextResultat[1].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[2].setOnAction(e->{
			joueurCourant.setScore(2, Integer.parseInt(tabTextResultat[2].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[3].setOnAction(e->{
			joueurCourant.setScore(3, Integer.parseInt(tabTextResultat[3].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[4].setOnAction(e->{
			joueurCourant.setScore(4, Integer.parseInt(tabTextResultat[4].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[5].setOnAction(e->{
			joueurCourant.setScore(5, Integer.parseInt(tabTextResultat[5].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[6].setOnAction(e->{
			joueurCourant.setScore(8, Integer.parseInt(tabTextResultat[6].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[7].setOnAction(e->{
			joueurCourant.setScore(9, Integer.parseInt(tabTextResultat[7].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[8].setOnAction(e->{
			joueurCourant.setScore(10, Integer.parseInt(tabTextResultat[8].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[9].setOnAction(e->{
			joueurCourant.setScore(11, Integer.parseInt(tabTextResultat[9].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[10].setOnAction(e->{
			joueurCourant.setScore(12, Integer.parseInt(tabTextResultat[10].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[11].setOnAction(e->{
			joueurCourant.setScore(13, Integer.parseInt(tabTextResultat[11].getText()));
			afficherScoresJoueur();
		});
		tabButtonResultat[12].setOnAction(e->{
			joueurCourant.setScore(14, Integer.parseInt(tabTextResultat[12].getText()));
			afficherScoresJoueur();
		});
		stage.setScene(scene);
		stage.setTitle("Yam's");
		stage.show();
	}

	/**
	 * Action effectu�e lors de la s�lection ou d�selection d'une checkBox. 
	 * Elle v�rifie que toutes les checkbox ne sont pas selectionn�es. Sinon elle d�sactive le
	 * bouton lancer.
	 */
	private void actionCheckBox() {
		boolean allSelected=true;
		for (byte i = 0; i < tabCheckBox.length; i++) {
			if(!tabCheckBox[i].isSelected())
				allSelected=false;
			if(allSelected)
				lancer.setDisable(true);
			else
				lancer.setDisable(false);
		}
	}

	/**
	 * Affiche les scores du joueur courant dans la grille de scores.
	 */
	private void afficherScoresJoueur() {
		int[] scoresJoueur = joueurCourant.getScores();
		for(byte i=0; i<scoresJoueur.length; i++) {
			gc.clearRect(170+joueurCourant.getNumero()*(220/nbJoueurs)-(joueurCourant.getNumero()*(220/nbJoueurs)-(joueurCourant.getNumero()-1)*220/nbJoueurs)/2-10, 30+(i+1)*31-10, 25, 13);
		}
		checkPrime(scoresJoueur);
		calculTotaux(scoresJoueur);
		for(byte i=0; i<scoresJoueur.length; i++) {
			if(scoresJoueur[i]!=-1)
				gc.strokeText(""+scoresJoueur[i], 170+joueurCourant.getNumero()*(220/nbJoueurs)-(joueurCourant.getNumero()*(220/nbJoueurs)-(joueurCourant.getNumero()-1)*220/nbJoueurs)/2-10, 30+(i+1)*31);
		}
		joueurSuivant();
	}

	/**
	 * M�thode execut�e apr�s qu'un joueur ait choisi o� affecter son score dans la grille.
	 * Elle permet de passer au joueur suivant et de r�initialiser les param�tres (compteur de lancers, textes
	 * et bloquage ou non des boutons de r�sultat en fonction de ce qu'il y a dans la grille du joueur).
	 */
	private void joueurSuivant() {
		int numJoueurSuivant = joueurCourant.getNumero();
		if(numJoueurSuivant>=tabJoueurs.length)
			numJoueurSuivant=0;
		joueurCourant = tabJoueurs[numJoueurSuivant];
		int[] scoresJoueur = joueurCourant.getScores();
		compteurLances = 3;
		textNblancers.setText(compteurLances+" lancés restants");
		lancer.setDisable(false);
		for(byte i=0; i < tabButtonResultat.length; i++) {
			tabButtonResultat[i].setDisable(false);
			tabTextResultat[i].setText("0");
		}
		for (byte i = 0; i < tabTextDes.length; i++) {
			tabTextDes[i].setText("0");
			tabGCDes[i].clearRect(0, 0, 30, 30);
			tabCheckBox[i].setSelected(false);
		}
		int idxButton;
		boolean fini = true;
		for (byte i = 0; i < scoresJoueur.length-2; i++) {
			if(i>7)
				idxButton = i-2;
			else
				idxButton = i;
			if(scoresJoueur[i]!=-1 && i!=6 && i!=7)
				tabButtonResultat[idxButton].setDisable(true);
			else if (scoresJoueur[i]==-1)
				fini = false;
		}
		textNomJoueur.setText("Au tour de : "+joueurCourant.getName());
		if(fini)
			finPartie();
	}

	/**
	 * Met fin � la partie, affichant le nom du vainqueur et son score. D�sactive tout les �l�ments avec lesquels
	 * le joueur pourrait interagir.
	 */
	private void finPartie() {
		lancer.setDisable(true);
		for(byte i = 0; i < tabCheckBox.length; i++)
			tabCheckBox[i].setDisable(true);
		int max = -1;
		int idxJoueur = 0;
		for (byte i = 0; i < nbJoueurs; i++) {
			if(tabJoueurs[i].getScores()[16] > max) {
				max = tabJoueurs[i].getScores()[16];
				idxJoueur = i;
			}
		}
		JOptionPane.showMessageDialog(null,  tabJoueurs[idxJoueur].getName()+" a remporté la victoire avec "+max+" points.\nBravo !", "Fin de la partie", JOptionPane.INFORMATION_MESSAGE);;
	}

	/**
	 * Calcul les 3 totaux du joueurs courant.
	 * @param scoresJoueur le tableau de scores du joueur courant.
	 */
	private void calculTotaux(int[] scoresJoueur) {
		int totalHaut = 0, totalBas = 0;
		for (byte i = 0; i < 7; i++) {
			if(scoresJoueur[i]!=-1)
				totalHaut+=scoresJoueur[i];
		}
		joueurCourant.setScore(7, totalHaut);
		for (byte i = 8; i < 15; i++) {
			if(scoresJoueur[i]!=-1)
				totalBas+=scoresJoueur[i];
		}
		joueurCourant.setScore(15, totalBas);
		joueurCourant.setScore(16, totalHaut+totalBas);
	}

	/**
	 * Calcule la prime du joueur en fonction du total de la partie haute de sa grille. Si > 62, alors
	 * prime de 35 points.
	 * @param scoresJoueur le tableau de scores du joueur courant.
	 */
	private void checkPrime(int[] scoresJoueur) {
		if(scoresJoueur[0]+scoresJoueur[1]+scoresJoueur[2]+scoresJoueur[3]+scoresJoueur[4]+scoresJoueur[5]>62)
			joueurCourant.setScore(6, 35);
		else
			joueurCourant.setScore(6, 0);
	}

	/**
	 * Calcule les r�sultats de chaque "figure" en fonction du lancer de d�s et les affiche en face du 
	 * bouton correspondant.
	 */
	private void calculResultats() {
		for (byte i = 0; i < 13; i++) {
			tabTextResultat[i].setText("0");
		}
		Set<Integer> set = new TreeSet<Integer>();
		ArrayList<Integer> list = new ArrayList<Integer>();
		int de1 = Integer.parseInt(tabTextDes[0].getText());
		int de2 = Integer.parseInt(tabTextDes[1].getText());
		int de3 = Integer.parseInt(tabTextDes[2].getText());
		int de4 = Integer.parseInt(tabTextDes[3].getText());
		int de5 = Integer.parseInt(tabTextDes[4].getText());
		int[] des = new int[] {de1, de2, de3, de4, de5};
		list.add(de1);
		list.add(de2);
		list.add(de3);
		list.add(de4);
		list.add(de5);
		set.addAll(list);
		//CALCUL DES TOTAUX POUR CHAQUE CHIFFRE
		calculerResultatsTotauxChiffres(des);
		//CHANCE = SOMME DES DES.
		tabTextResultat[12].setText(""+(de1+de2+de3+de4+de5));
		if(set.size() == 1) { //SI 1 SEULE VALEUR: YAM'S (FORCEMENT BRELAN ET CARRE)
			tabTextResultat[11].setText("50");
			tabTextResultat[8].setText(""+de1*4);
			tabTextResultat[6].setText(""+de1*3);
		}
		else if(set.size() == 5) { //SI 5 VALEURS DIFFERENTES: PETITE OU GRANDE SUITE
			Set<Integer> setGrandeSuite = new TreeSet<Integer>();
			setGrandeSuite.add(2);
			setGrandeSuite.add(3);
			setGrandeSuite.add(4);
			setGrandeSuite.add(5);
			setGrandeSuite.add(6);
			Set<Integer> setPetiteSuite = new TreeSet<Integer>();
			setPetiteSuite.add(1);
			setPetiteSuite.add(2);
			setPetiteSuite.add(3);
			setPetiteSuite.add(4);
			setPetiteSuite.add(5);
			if(set.containsAll(setGrandeSuite))
				tabTextResultat[10].setText("40");
			else if(set.containsAll(setPetiteSuite))
				tabTextResultat[9].setText("30");
		}
		else if(set.size() == 2) { //SI 2 VALEURS DIFFERENTES: CARRE OU FULL (FORCEMENT BRELAN)
			//ON REGARDE SI ON EST DANS LE CAS XXXXY OU XXXYY
			int[] cptXY = new int[] {1, 0};
			int XouY = 0;
			int dePrec=0;
			boolean premier = true;
			for (int de : list) {
				if(!premier) {
					if(de == dePrec)
						cptXY[XouY]++;
					else {
						if(XouY == 0)
							XouY = 1;
						else
							XouY = 0;
						cptXY[XouY]++;
					}
				}
				dePrec=de;
				premier=false;
			}
			//CARRE DU CHIFFRE DU DE1
			if(cptXY[0] == 4) {
				tabTextResultat[8].setText(""+de1*4);
				tabTextResultat[6].setText(""+de1*3);
			}
			//CARRE DU CHIFFRE QUI N'EST PAS LE DE1
			else if(cptXY[1] == 4) {
				tabTextResultat[8].setText(""+de2*4);
				tabTextResultat[6].setText(""+de2*3);
			}
			//SI XXXYY ALORS FULL ET BRELAN
			else if(cptXY[0] == 3 || cptXY[1] == 3)	{
				tabTextResultat[7].setText("25");
				//BRELAN DU CHIFFRE DU DE1
				if(cptXY[0] == 3)
					tabTextResultat[6].setText(""+de1*3);
				//BRELAN DU CHIFFRE DU DE2 ET DE3
				else {
					if(de2==de3)
						tabTextResultat[6].setText(""+de2*3);
					//BRELAN DU CHIFFRE DU DE4
					else
						tabTextResultat[6].setText(""+de4*3);
				}
			}
		}
		else if(set.size()== 3) { //SI 3 VALEURS DIFFERENTES: POSSIBILITE BRELAN SI XXXYZ ET NON PAS XXYYZ
			int nombreBrelan = 0;
			//ON REGARDE POUR QUEL NOMBRE ON A UN BRELAN
			int[] nombres = new int[] {0, 0, 0, 0, 0, 0};
			for (int de : list) {
				if(de!=0)
					nombres[de-1]++;
			}
			//ON REGARDE SI IL Y A BRELAN
			while(nombreBrelan<6 && nombres[nombreBrelan]!=3) {
				nombreBrelan++;
			}
			if(nombreBrelan<6)
				tabTextResultat[6].setText(""+(3*(nombreBrelan+1)));
		}
	}

	/**
	 * Calcule les r�sultats pour les figures du haut de la grille, � savoir les totaux pour chaque
	 * chiffre.
	 * @param des les r�sultats des d�s
	 */
	private void calculerResultatsTotauxChiffres(int[] des) {
		for (int de : des) {
			switch (de) {
			case 1:
				tabTextResultat[0].setText(""+(Integer.parseInt(tabTextResultat[0].getText())+1));
				break;
			case 2:
				tabTextResultat[1].setText(""+(Integer.parseInt(tabTextResultat[1].getText())+2));
				break;
			case 3:
				tabTextResultat[2].setText(""+(Integer.parseInt(tabTextResultat[2].getText())+3));
				break;
			case 4:
				tabTextResultat[3].setText(""+(Integer.parseInt(tabTextResultat[3].getText())+4));
				break;
			case 5:
				tabTextResultat[4].setText(""+(Integer.parseInt(tabTextResultat[4].getText())+5));
				break;
			case 6:
				tabTextResultat[5].setText(""+(Integer.parseInt(tabTextResultat[5].getText())+6));
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Tire un nombre al�atoire entre 1 et 6.
	 * @return un nombre al�atoire entre 1 et 6.
	 */
	public byte tirage() {
		Random r = new Random();
		return (byte) (r.nextInt(6)+1);
	}

	/**
	 * Demande aux joueurs leur nombre.
	 * @return le nombre de joueur
	 */
	private int nombreJoueurs() {
		String nb="";
		do {
			nb = JOptionPane.showInputDialog(null, "Saisissez le nombre de joueurs entre 2 et 6", "Nombre de joueurs", JOptionPane.QUESTION_MESSAGE);
			if(nb==null || !nb.matches("[2-6]"))
				nb="0";
		}while(Integer.parseInt(nb) < 2 || Integer.parseInt(nb) > 6);
		return Integer.parseInt(nb);
	}

	/**
	 * Demande � un joueur son nom.
	 * @param num le numero du joueur
	 * @return le nouveau Joueur
	 */
	private Joueur nouveauJoueur(int num) {
		String nom = JOptionPane.showInputDialog(null, "Saisissez le prénom du joueur "+num+" (18 caractères max)", "Joueur "+num, JOptionPane.QUESTION_MESSAGE);
		if(nom.length() < 1 || nom.length() > 18)
			nom="Joueur "+num;
		return new Joueur(nom, num);
	}

	/**
	 * M�thode principale lan�ant le programme.
	 * @param args param�tres du programme.
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}

	/**
	 * Affichage d'un d�filement de nombre al�atoire lors du tirage al�atoire (non fonctionnel).
	 */
	public synchronized void runThreadSync() {
		// TODO Auto-generated method stub
		lancer.setDisable(true);
		for (byte i = 0; i < 5; i++) {
			if(!tabCheckBox[i].isSelected()) {
				byte nb_tire=tirage();
				tabTextDes[i].setText(""+nb_tire);
				tabGCDes[i].drawImage(new Image(new File("ressources/de_"+nb_tire+".png").toURI().toString()), 0, 0, 20, 20);
			}
		}
		calculResultats();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {}
		lancer.setDisable(false);
		if(compteurLances < 1)
			lancer.setDisable(true);
	}
}
